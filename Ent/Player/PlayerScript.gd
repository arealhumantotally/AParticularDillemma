extends KinematicBody2D
class_name Player

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var horivelocity = 0
var vertivelocity = 0
var speed = 200
var gravity = 100
var terminalvelocity = -200
var jumpheight = -100
# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#I originally wanted something more complex, but that proved to take too much time. So this will do for now.
	if Input.is_action_pressed("ui_left"):
			horivelocity = speed * -1
	elif Input.is_action_pressed("ui_right"):
		horivelocity = speed 
	if Input.is_action_pressed("ui_up"):
		if is_on_floor():
			vertivelocity = jumpheight
	if Input.is_action_pressed("Interact"):
		for i in $CollisionShape2D/Area2D.get_overlapping_areas():
			if i is Interactable:
				i.Interact(self)
	move_and_slide(Vector2(horivelocity, vertivelocity), Vector2.UP)
	horivelocity = 0
	vertivelocity = max(terminalvelocity, vertivelocity + (gravity * delta))
