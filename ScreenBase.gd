extends Node2D
class_name Level
#Sets up the exits.
export(PackedScene) var InitLeftExit
onready var LeftExit = $InitLeftExit
export(PackedScene) var InitRightExit
onready var RightExit = $InitRightExit
export(PackedScene) var InitDownExit
onready var DownExit = $InitDownExit
export(PackedScene) var InitUpExit
onready var UpExit = $InitUpExit
# All screens must have a camera.
onready var cam = $Camera2D
export(bool) var leavable

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
